#ligas wikidata

## paginas
Lista de propiedades mas generales
[Lista de propiedades](https://www.wikidata.org/wiki/Wikidata:List_of_properties/es#:~:text=En%20esta%20p%C3%A1gina%20encontrar%C3%A1s%20varias,caja%20de%20entrada%20de%20abajo.)

WikiProject Music

Tiene todas las posibles propiedades para grupos y musicos
[WikiProject Music](
https://www.wikidata.org/wiki/Wikidata:WikiProject_Music)

Wikidata:Glosario
[Lista de terminos usados](
https://www.wikidata.org/wiki/Wikidata:Glossary/es)

## Herramientas en wikidata
Borrando de elementos
https://www.wikidata.org/wiki/Wikidata:Requests_for_deletions

## Herramientas externas

Para tener un mapa y agregar datos o imagenes que falten


[https://wikishootme.toolforge.org/](
https://wikishootme.toolforge.org/)

Visualizador de conexiones

[https://angryloki.github.io/wikidata-graph-builder/](https://angryloki.github.io/wikidata-graph-builder/)

Visualización en arbol
[https://www.entitree.com](
https://www.entitree.com)


##videos
Introducción a wikidata
https://www.youtube.com/watch?v=LCo0SRsci2w&list=PLFKNtUouDusxltogjz11NoMmCE264YEL9